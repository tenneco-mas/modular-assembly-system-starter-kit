USE [master]
GO
/****** Object:  Database [FM_35710]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE DATABASE [FM_35710]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FM_35710', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\FM_35710.mdf' , SIZE = 623424KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'FM_35710_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\FM_35710_log.ldf' , SIZE = 757248KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [FM_35710] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FM_35710].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FM_35710] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FM_35710] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FM_35710] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FM_35710] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FM_35710] SET ARITHABORT OFF 
GO
ALTER DATABASE [FM_35710] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FM_35710] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FM_35710] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FM_35710] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FM_35710] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FM_35710] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FM_35710] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FM_35710] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FM_35710] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FM_35710] SET  DISABLE_BROKER 
GO
ALTER DATABASE [FM_35710] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FM_35710] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FM_35710] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FM_35710] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FM_35710] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FM_35710] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FM_35710] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FM_35710] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [FM_35710] SET  MULTI_USER 
GO
ALTER DATABASE [FM_35710] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FM_35710] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FM_35710] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FM_35710] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [FM_35710] SET DELAYED_DURABILITY = DISABLED 
GO
USE [FM_35710]
GO
/****** Object:  User [fm]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE USER [fm] FOR LOGIN [fm] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [fm]
GO
ALTER ROLE [db_datareader] ADD MEMBER [fm]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [fm]
GO
/****** Object:  UserDefinedTableType [dbo].[Result]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE TYPE [dbo].[Result] AS TABLE(
	[Result_Id] [bigint] NULL,
	[Result_Value] [varchar](200) NULL
)
GO
/****** Object:  UserDefinedFunction [dbo].[splitstring]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create FUNCTION [dbo].[splitstring] ( @stringToSplit VARCHAR(MAX) )
RETURNS
 @returnList TABLE ([item] [nvarchar] (500))
AS
BEGIN

 DECLARE @item NVARCHAR(255)
 DECLARE @pos INT

 WHILE CHARINDEX(',', @stringToSplit) > 0
 BEGIN
  SELECT @pos  = CHARINDEX(',', @stringToSplit)  
  SELECT @item = SUBSTRING(@stringToSplit, 1, @pos-1)

  INSERT INTO @returnList 
  SELECT @item

  SELECT @stringToSplit = SUBSTRING(@stringToSplit, @pos+1, LEN(@stringToSplit)-@pos)
 END

 INSERT INTO @returnList
 SELECT @stringToSplit

 RETURN
END
GO
/****** Object:  Table [dbo].[TB_JOBS]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_JOBS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Fk_Pieces] [bigint] NOT NULL,
	[Fk_Stations] [bigint] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[JobTime] [smallint] NOT NULL,
	[CycleTime] [smallint] NOT NULL,
	[excluded] [bit] NOT NULL,
 CONSTRAINT [PK_TB_JOBS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_LOTS]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_LOTS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Fk_Parts] [bigint] NOT NULL,
	[LotNumber] [varchar](100) NOT NULL,
	[StartDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TB_LOTS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_MACHINE_PARAMETERS_VALUES]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_MACHINE_PARAMETERS_VALUES](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Value] [varchar](100) NOT NULL,
	[Tag] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TB_MACHINE_PARAMETERS_VALUES] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_MODULES_CONFIG]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_MODULES_CONFIG](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[moduleName] [varchar](255) NOT NULL,
	[ipAddress] [varchar](15) NOT NULL,
	[moduleDescription] [varchar](255) NOT NULL,
	[moduleOrder] [int] NOT NULL,
	[hidden] [bit] NOT NULL,
	[canBeExcluded] [bit] NOT NULL,
 CONSTRAINT [PK_modules] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_MODULES_STATUS]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_MODULES_STATUS](
	[Fk_Modules] [bigint] NOT NULL,
	[Excluded] [bit] NOT NULL,
 CONSTRAINT [PK_TB_MODULES_STATUS] PRIMARY KEY CLUSTERED 
(
	[Fk_Modules] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_NEXTPATHS_CONFIG]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_NEXTPATHS_CONFIG](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[pathId] [bigint] NOT NULL,
	[nextPathId] [bigint] NOT NULL,
 CONSTRAINT [PK_TB_NEXTPATHS_CONFIG] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_PARAMETERS_INFO]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_PARAMETERS_INFO](
	[Id_Parameter_Info] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[DataType] [varchar](100) NOT NULL,
	[Unit] [varchar](50) NOT NULL,
	[Tag] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TB_PARAMETERS_INFO] PRIMARY KEY CLUSTERED 
(
	[Id_Parameter_Info] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_PARAMETERS_VALUES]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_PARAMETERS_VALUES](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Fk_Parameters_Info] [bigint] NOT NULL,
	[Fk_Parts] [bigint] NOT NULL,
	[Value] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TB_PARAMETERS_VALUES] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_PARTS]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_PARTS](
	[Id_Parts] [bigint] IDENTITY(1,1) NOT NULL,
	[PartNumber] [varchar](100) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[ToolSet] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TB_PARTS] PRIMARY KEY CLUSTERED 
(
	[Id_Parts] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_PATHS_CONFIG]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_PATHS_CONFIG](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[pathId] [int] NOT NULL,
	[Fk_Modules] [bigint] NOT NULL,
 CONSTRAINT [PK_paths] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_PIECES]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_PIECES](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[serialNumber] [varchar](50) NOT NULL,
	[Fk_Lots] [bigint] NOT NULL,
	[lastWorkedStation] [bigint] NOT NULL,
	[loadVehicleTag] [varchar](50) NOT NULL,
	[vehicleTag] [varchar](50) NOT NULL,
	[state] [tinyint] NOT NULL,
	[orientation] [tinyint] NOT NULL,
	[Fk_ScrapCodes_Info] [bigint] NULL,
	[LoadDate] [datetime] NOT NULL,
	[UnloadDate] [datetime] NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_pieces] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_REQUIREMENTS_CONFIG]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_REQUIREMENTS_CONFIG](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[stationId] [bigint] NOT NULL,
	[reqStationId] [bigint] NOT NULL,
	[reqState] [tinyint] NOT NULL,
	[reqOrientation] [tinyint] NOT NULL,
 CONSTRAINT [PK_requirements] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_RESULTS]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_RESULTS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Fk_Pieces] [bigint] NOT NULL,
	[Fk_Results_Info] [bigint] NOT NULL,
	[Fk_Jobs] [bigint] NOT NULL,
	[Value] [varchar](200) NOT NULL,
 CONSTRAINT [PK_TB_RESULTS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_RESULTS_INFO]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_RESULTS_INFO](
	[Id_Result_Info] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[DataType] [varchar](100) NOT NULL,
	[Unit] [varchar](50) NOT NULL,
	[Tag] [varchar](100) NOT NULL,
	[Hidden] [bit] NOT NULL,
 CONSTRAINT [PK_TB_RESULTS_INFO] PRIMARY KEY CLUSTERED 
(
	[Id_Result_Info] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_SCRAPCODES_INFO]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_SCRAPCODES_INFO](
	[Id_ScrapCode_Info] [bigint] IDENTITY(1,1) NOT NULL,
	[ScrapCode] [int] NOT NULL,
	[Description] [varchar](100) NOT NULL,
	[Fk_Stations] [bigint] NOT NULL,
 CONSTRAINT [PK_TB_SCRAPCODES_INFO] PRIMARY KEY CLUSTERED 
(
	[Id_ScrapCode_Info] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_STATIONS_CONFIG]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_STATIONS_CONFIG](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[description] [varchar](50) NOT NULL,
	[stationOrder] [int] NOT NULL,
	[Fk_Modules] [bigint] NULL,
	[Fk_Paths] [bigint] NOT NULL,
	[position] [real] NOT NULL,
	[alwaysStop] [bit] NOT NULL,
	[isLoad] [bit] NOT NULL,
	[isUnload] [bit] NOT NULL,
	[isVirtual] [bit] NOT NULL,
	[isBackStation] [bit] NOT NULL,
	[isReworkStation] [bit] NOT NULL,
	[canBeExcluded] [bit] NOT NULL,
	[hasTagReader] [bit] NOT NULL,
	[isPreToolChanger] [bit] NOT NULL,
	[isToolChanger] [bit] NOT NULL,
	[isToolChecker] [bit] NOT NULL,
	[isMaintenanceStation] [bit] NOT NULL,
	[isCounterStation] [bit] NOT NULL,
	[masterStationId] [bigint] NOT NULL,
	[virtualJobTime] [int] NOT NULL,
	[slaveindex] [int] NOT NULL,
	[groupMasterStationId] [bigint] NOT NULL,
	[groupIndex] [int] NOT NULL,
	[RestVehicle] [bit] NOT NULL,
	[traficStationId] [bigint] NOT NULL,
 CONSTRAINT [PK_Stations] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_STATIONS_STATUS]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_STATIONS_STATUS](
	[Fk_Stations] [bigint] NOT NULL,
	[Excluded] [bit] NOT NULL,
 CONSTRAINT [PK_TB_STATIONS_STATUS] PRIMARY KEY CLUSTERED 
(
	[Fk_Stations] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_USERS]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_USERS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[userLevel] [tinyint] NOT NULL,
 CONSTRAINT [PK_TB_USERS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_VEHICLE_INSTANCES]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_VEHICLE_INSTANCES](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[vehicleId] [smallint] NOT NULL,
	[vehicleInstance] [tinyint] NOT NULL,
 CONSTRAINT [PK_vehicleInstances] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TB_VEHICLES_STATUS]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TB_VEHICLES_STATUS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[tag] [varchar](50) NOT NULL,
	[Excluded] [bit] NOT NULL,
	[Fk_Parts] [bigint] NOT NULL,
	[ForceGoToStationId] [bigint] NOT NULL,
	[NeedMaintenance] [bit] NOT NULL,
 CONSTRAINT [PK_TB_VEHICLES_STATUS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[V000_Parameter_Values]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V000_Parameter_Values]
AS
SELECT        dbo.TB_PARTS.PartNumber, dbo.TB_PARTS.Id_Parts, dbo.TB_PARAMETERS_INFO.Id_Parameter_Info, dbo.TB_PARAMETERS_INFO.Description AS ParameterDescription, dbo.TB_PARAMETERS_VALUES.Value, 
                         dbo.TB_PARAMETERS_INFO.DataType
FROM            dbo.TB_PARTS LEFT OUTER JOIN
                         dbo.TB_PARAMETERS_VALUES ON dbo.TB_PARTS.Id_Parts = dbo.TB_PARAMETERS_VALUES.Fk_Parts RIGHT OUTER JOIN
                         dbo.TB_PARAMETERS_INFO ON dbo.TB_PARAMETERS_VALUES.Fk_Parameters_Info = dbo.TB_PARAMETERS_INFO.Id_Parameter_Info
GO
/****** Object:  View [dbo].[V001_Result_Values]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V001_Result_Values]
AS
SELECT dbo.TB_RESULTS_INFO.Id_Result_Info, dbo.TB_PIECES.serialNumber, dbo.TB_RESULTS.Value, dbo.TB_RESULTS_INFO.DataType, dbo.TB_RESULTS_INFO.Description, dbo.TB_PIECES.id
FROM     dbo.TB_PIECES LEFT OUTER JOIN
                  dbo.TB_RESULTS ON dbo.TB_PIECES.id = dbo.TB_RESULTS.Fk_Pieces RIGHT OUTER JOIN
                  dbo.TB_RESULTS_INFO ON dbo.TB_RESULTS.Fk_Results_Info = dbo.TB_RESULTS_INFO.Id_Result_Info
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [keylots_lotNumber]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [keylots_lotNumber] ON [dbo].[TB_LOTS]
(
	[LotNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [keyParameters_Tag]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [keyParameters_Tag] ON [dbo].[TB_MACHINE_PARAMETERS_VALUES]
(
	[Tag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [keyParameters_tag]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [keyParameters_tag] ON [dbo].[TB_PARAMETERS_INFO]
(
	[Tag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [keyparts_partNumber]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [keyparts_partNumber] ON [dbo].[TB_PARTS]
(
	[PartNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
/****** Object:  Index [keypaths_pathId]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [keypaths_pathId] ON [dbo].[TB_PATHS_CONFIG]
(
	[pathId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [keypieces_serialNumber]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [keypieces_serialNumber] ON [dbo].[TB_PIECES]
(
	[serialNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
/****** Object:  Index [keypieces_UnloadDate]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE NONCLUSTERED INDEX [keypieces_UnloadDate] ON [dbo].[TB_PIECES]
(
	[UnloadDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [keypieces_vehicleTag]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE NONCLUSTERED INDEX [keypieces_vehicleTag] ON [dbo].[TB_PIECES]
(
	[vehicleTag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
/****** Object:  Index [NonClusteredIndex-20200630-145034]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200630-145034] ON [dbo].[TB_RESULTS]
(
	[Fk_Pieces] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [NonClusteredIndex-20200630-145056]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20200630-145056] ON [dbo].[TB_RESULTS]
(
	[Fk_Results_Info] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [keyResults_tag]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [keyResults_tag] ON [dbo].[TB_RESULTS_INFO]
(
	[Tag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
/****** Object:  Index [keyscraps_codeStation]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [keyscraps_codeStation] ON [dbo].[TB_SCRAPCODES_INFO]
(
	[ScrapCode] ASC,
	[Fk_Stations] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [keyUsers_name]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [keyUsers_name] ON [dbo].[TB_USERS]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
/****** Object:  Index [keyvehicleInstances_vehicleId]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [keyvehicleInstances_vehicleId] ON [dbo].[TB_VEHICLE_INSTANCES]
(
	[vehicleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [keyVehicles_tag]    Script Date: 11/30/2020 4:33:52 PM ******/
CREATE NONCLUSTERED INDEX [keyVehicles_tag] ON [dbo].[TB_VEHICLES_STATUS]
(
	[tag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TB_PATHS_CONFIG] ADD  CONSTRAINT [DF_TB_PATHS_CONFIG_pathId]  DEFAULT ((0)) FOR [pathId]
GO
ALTER TABLE [dbo].[TB_PATHS_CONFIG] ADD  CONSTRAINT [DF_TB_PATHS_CONFIG_Fk_Modules]  DEFAULT ((0)) FOR [Fk_Modules]
GO
ALTER TABLE [dbo].[TB_PIECES] ADD  CONSTRAINT [DF_TB_PIECES_Fk_Lots]  DEFAULT ((0)) FOR [Fk_Lots]
GO
ALTER TABLE [dbo].[TB_JOBS]  WITH CHECK ADD  CONSTRAINT [FK_TB_JOBS_TB_PIECES] FOREIGN KEY([Fk_Pieces])
REFERENCES [dbo].[TB_PIECES] ([id])
GO
ALTER TABLE [dbo].[TB_JOBS] CHECK CONSTRAINT [FK_TB_JOBS_TB_PIECES]
GO
ALTER TABLE [dbo].[TB_JOBS]  WITH CHECK ADD  CONSTRAINT [FK_TB_JOBS_TB_STATIONS_CONFIG] FOREIGN KEY([Fk_Stations])
REFERENCES [dbo].[TB_STATIONS_CONFIG] ([id])
GO
ALTER TABLE [dbo].[TB_JOBS] CHECK CONSTRAINT [FK_TB_JOBS_TB_STATIONS_CONFIG]
GO
ALTER TABLE [dbo].[TB_LOTS]  WITH CHECK ADD  CONSTRAINT [FK_TB_LOTS_TB_PARTS] FOREIGN KEY([Fk_Parts])
REFERENCES [dbo].[TB_PARTS] ([Id_Parts])
GO
ALTER TABLE [dbo].[TB_LOTS] CHECK CONSTRAINT [FK_TB_LOTS_TB_PARTS]
GO
ALTER TABLE [dbo].[TB_MODULES_STATUS]  WITH CHECK ADD  CONSTRAINT [FK_TB_MODULES_STATUS_TB_MODULES_CONFIG] FOREIGN KEY([Fk_Modules])
REFERENCES [dbo].[TB_MODULES_CONFIG] ([id])
GO
ALTER TABLE [dbo].[TB_MODULES_STATUS] CHECK CONSTRAINT [FK_TB_MODULES_STATUS_TB_MODULES_CONFIG]
GO
ALTER TABLE [dbo].[TB_NEXTPATHS_CONFIG]  WITH CHECK ADD  CONSTRAINT [FK_TB_NEXTPATHS_CONFIG_TB_PATHS_CONFIG] FOREIGN KEY([pathId])
REFERENCES [dbo].[TB_PATHS_CONFIG] ([id])
GO
ALTER TABLE [dbo].[TB_NEXTPATHS_CONFIG] CHECK CONSTRAINT [FK_TB_NEXTPATHS_CONFIG_TB_PATHS_CONFIG]
GO
ALTER TABLE [dbo].[TB_NEXTPATHS_CONFIG]  WITH CHECK ADD  CONSTRAINT [FK_TB_NEXTPATHS_CONFIG_TB_PATHS_CONFIG1] FOREIGN KEY([nextPathId])
REFERENCES [dbo].[TB_PATHS_CONFIG] ([id])
GO
ALTER TABLE [dbo].[TB_NEXTPATHS_CONFIG] CHECK CONSTRAINT [FK_TB_NEXTPATHS_CONFIG_TB_PATHS_CONFIG1]
GO
ALTER TABLE [dbo].[TB_PARAMETERS_VALUES]  WITH CHECK ADD  CONSTRAINT [FK_TB_PARAMETERS_VALUES_TB_PARAMETERS_INFO] FOREIGN KEY([Fk_Parameters_Info])
REFERENCES [dbo].[TB_PARAMETERS_INFO] ([Id_Parameter_Info])
GO
ALTER TABLE [dbo].[TB_PARAMETERS_VALUES] CHECK CONSTRAINT [FK_TB_PARAMETERS_VALUES_TB_PARAMETERS_INFO]
GO
ALTER TABLE [dbo].[TB_PARAMETERS_VALUES]  WITH CHECK ADD  CONSTRAINT [FK_TB_PARAMETERS_VALUES_TB_PARTS] FOREIGN KEY([Fk_Parts])
REFERENCES [dbo].[TB_PARTS] ([Id_Parts])
GO
ALTER TABLE [dbo].[TB_PARAMETERS_VALUES] CHECK CONSTRAINT [FK_TB_PARAMETERS_VALUES_TB_PARTS]
GO
ALTER TABLE [dbo].[TB_PATHS_CONFIG]  WITH CHECK ADD  CONSTRAINT [FK_TB_PATHS_CONFIG_TB_MODULES_CONFIG] FOREIGN KEY([Fk_Modules])
REFERENCES [dbo].[TB_MODULES_CONFIG] ([id])
GO
ALTER TABLE [dbo].[TB_PATHS_CONFIG] CHECK CONSTRAINT [FK_TB_PATHS_CONFIG_TB_MODULES_CONFIG]
GO
ALTER TABLE [dbo].[TB_PIECES]  WITH CHECK ADD  CONSTRAINT [FK_TB_PIECES_TB_PIECES] FOREIGN KEY([id])
REFERENCES [dbo].[TB_PIECES] ([id])
GO
ALTER TABLE [dbo].[TB_PIECES] CHECK CONSTRAINT [FK_TB_PIECES_TB_PIECES]
GO
ALTER TABLE [dbo].[TB_PIECES]  WITH CHECK ADD  CONSTRAINT [FK_TB_PIECES_TB_SCRAPCODES_INFO] FOREIGN KEY([Fk_ScrapCodes_Info])
REFERENCES [dbo].[TB_SCRAPCODES_INFO] ([Id_ScrapCode_Info])
GO
ALTER TABLE [dbo].[TB_PIECES] CHECK CONSTRAINT [FK_TB_PIECES_TB_SCRAPCODES_INFO]
GO
ALTER TABLE [dbo].[TB_REQUIREMENTS_CONFIG]  WITH CHECK ADD  CONSTRAINT [FK_TB_REQUIREMENTS_CONFIG_TB_STATIONS_CONFIG] FOREIGN KEY([stationId])
REFERENCES [dbo].[TB_STATIONS_CONFIG] ([id])
GO
ALTER TABLE [dbo].[TB_REQUIREMENTS_CONFIG] CHECK CONSTRAINT [FK_TB_REQUIREMENTS_CONFIG_TB_STATIONS_CONFIG]
GO
ALTER TABLE [dbo].[TB_REQUIREMENTS_CONFIG]  WITH CHECK ADD  CONSTRAINT [FK_TB_REQUIREMENTS_CONFIG_TB_STATIONS_CONFIG1] FOREIGN KEY([reqStationId])
REFERENCES [dbo].[TB_STATIONS_CONFIG] ([id])
GO
ALTER TABLE [dbo].[TB_REQUIREMENTS_CONFIG] CHECK CONSTRAINT [FK_TB_REQUIREMENTS_CONFIG_TB_STATIONS_CONFIG1]
GO
ALTER TABLE [dbo].[TB_RESULTS]  WITH CHECK ADD  CONSTRAINT [FK_TB_RESULTS_TB_PIECES] FOREIGN KEY([Fk_Pieces])
REFERENCES [dbo].[TB_PIECES] ([id])
GO
ALTER TABLE [dbo].[TB_RESULTS] CHECK CONSTRAINT [FK_TB_RESULTS_TB_PIECES]
GO
ALTER TABLE [dbo].[TB_RESULTS]  WITH CHECK ADD  CONSTRAINT [FK_TB_RESULTS_TB_RESULTS_INFO] FOREIGN KEY([Fk_Results_Info])
REFERENCES [dbo].[TB_RESULTS_INFO] ([Id_Result_Info])
GO
ALTER TABLE [dbo].[TB_RESULTS] CHECK CONSTRAINT [FK_TB_RESULTS_TB_RESULTS_INFO]
GO
ALTER TABLE [dbo].[TB_SCRAPCODES_INFO]  WITH CHECK ADD  CONSTRAINT [FK_TB_SCRAPCODES_INFO_TB_STATIONS_CONFIG] FOREIGN KEY([Fk_Stations])
REFERENCES [dbo].[TB_STATIONS_CONFIG] ([id])
GO
ALTER TABLE [dbo].[TB_SCRAPCODES_INFO] CHECK CONSTRAINT [FK_TB_SCRAPCODES_INFO_TB_STATIONS_CONFIG]
GO
ALTER TABLE [dbo].[TB_STATIONS_CONFIG]  WITH CHECK ADD  CONSTRAINT [FK_TB_STATIONS_CONFIG_TB_MODULES_CONFIG] FOREIGN KEY([Fk_Modules])
REFERENCES [dbo].[TB_MODULES_CONFIG] ([id])
GO
ALTER TABLE [dbo].[TB_STATIONS_CONFIG] CHECK CONSTRAINT [FK_TB_STATIONS_CONFIG_TB_MODULES_CONFIG]
GO
ALTER TABLE [dbo].[TB_STATIONS_CONFIG]  WITH CHECK ADD  CONSTRAINT [FK_TB_STATIONS_CONFIG_TB_PATHS_CONFIG] FOREIGN KEY([Fk_Paths])
REFERENCES [dbo].[TB_PATHS_CONFIG] ([id])
GO
ALTER TABLE [dbo].[TB_STATIONS_CONFIG] CHECK CONSTRAINT [FK_TB_STATIONS_CONFIG_TB_PATHS_CONFIG]
GO
ALTER TABLE [dbo].[TB_STATIONS_STATUS]  WITH CHECK ADD  CONSTRAINT [FK_TB_STATIONS_STATUS_TB_STATIONS_CONFIG] FOREIGN KEY([Fk_Stations])
REFERENCES [dbo].[TB_STATIONS_CONFIG] ([id])
GO
ALTER TABLE [dbo].[TB_STATIONS_STATUS] CHECK CONSTRAINT [FK_TB_STATIONS_STATUS_TB_STATIONS_CONFIG]
GO
/****** Object:  StoredProcedure [dbo].[stp_add_Job]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_add_Job]
	-- Add the parameters for the stored procedure here
		@Serial_Number VARCHAR(100),
		@Station_Id BIGINT,
		@JobTime SMALLINT,
		@CycleTime SMALLINT,
		@Excluded BIT,
		@Job_Id BIGINT OUTPUT

AS
BEGIN

	DECLARE @ID_PIECES INT
	SET @ID_PIECES =(SELECT TOP(1) id FROM dbo.TB_PIECES  WHERE serialNumber = @Serial_Number )
	if (@ID_PIECES IS NOT NULL)
		BEGIN
			INSERT dbo.TB_JOBS
				(
					Fk_Pieces,
					Fk_Stations,
					TimeStamp,
					JobTime,
					CycleTime,
					excluded
				)
			VALUES
				(
					@ID_PIECES, -- Fk_Pieces - BIGINT
					@Station_Id,  -- Fk_Stations - BIGINT
					GETDATE(), -- TimeStamp - DATETIME
					@JobTime, -- JobTime - SMALLINT
					@CycleTime, -- CycleTime - SMALLINT
					@Excluded -- excluded bit
				)
			SET @Job_Id = SCOPE_IDENTITY()
		END
	ELSE
		BEGIN
			SET @Job_Id = 0
		END
END
GO
/****** Object:  StoredProcedure [dbo].[stp_add_Lot]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_add_Lot]
	-- Add the parameters for the stored procedure here
		@Lot_Number VARCHAR(100),
		@Part_Number VARCHAR(100),
		@Lot_Id BIGINT OUT
AS
BEGIN
    DECLARE @ID_PARTS AS BIGINT
	SET @ID_PARTS =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)
	IF(@ID_PARTS IS NULL)
			BEGIN
			SET @Lot_Id = 0
		END
	ELSE
	BEGIN
	    DECLARE @ID AS BIGINT
	    DECLARE @ID_PARTS2 AS BIGINT
		SELECT TOP(1) @ID=id, @ID_PARTS2 = Fk_Parts FROM dbo.TB_LOTS WHERE LotNumber = @Lot_Number
		IF(@ID IS NULL)
			BEGIN
    
				INSERT dbo.TB_LOTS
					(
						LotNumber,
						Fk_Parts,
						StartDate 
					)
				VALUES
					(
						@Lot_Number, -- LotNumber - varchar(100)
						@ID_PARTS,  -- Fk_Parts - bigint
						GetDATE() -- StartDate - DateTime
					)
				SET @Lot_Id = SCOPE_IDENTITY()
			END
		ELSE
			BEGIN
				if (@ID_PARTS2 = @ID_PARTS)
					BEGIN
						SET @Lot_Id = @ID
					END
			END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[stp_add_MachineParameter]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_add_MachineParameter]
	-- Add the parameters for the stored procedure here
		@Parameter_Tag VARCHAR(100),
		@Parameter_Id BIGINT OUTPUT

AS
BEGIN

	DECLARE @ID BIGINT
	SET @ID =(SELECT TOP(1) id FROM dbo.TB_MACHINE_PARAMETERS_VALUES  WHERE TB_MACHINE_PARAMETERS_VALUES.Tag = @Parameter_Tag )

	IF(@ID IS NULL)
	BEGIN
		INSERT dbo.TB_MACHINE_PARAMETERS_VALUES
			(
			    Tag,
				Value 
			)
		VALUES
			(
				@Parameter_Tag, -- tag - varchar(100)
				'' -- Value - varchar(100)
			)
		SET @Parameter_Id = SCOPE_IDENTITY()

	END
	ELSE		
	BEGIN
		SET @Parameter_Id = @ID
	END   
END
GO
/****** Object:  StoredProcedure [dbo].[stp_add_Parameter]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_add_Parameter]
	-- Add the parameters for the stored procedure here
		@Parameter_Description VARCHAR(100),
		@Parameter_Type VARCHAR(100),
		@Parameter_Unit VARCHAR(50),
		@Parameter_Tag VARCHAR(100),
		@Parameter_Id BIGINT OUTPUT

AS
BEGIN

	DECLARE @ID BIGINT
	SET @ID =(SELECT TOP(1)Id_Parameter_Info FROM dbo.TB_PARAMETERS_INFO WHERE TB_PARAMETERS_INFO.Tag= @Parameter_Tag)

	IF(@ID IS NULL)
	BEGIN
		INSERT dbo.TB_PARAMETERS_INFO
			(
				Description,
				DataType,
				Unit,
				Tag
			)
		VALUES
			(
				@Parameter_Description, -- Description - varchar(200)
				@Parameter_Type,  -- DataType - varchar(100)
				@Parameter_Unit, -- Unit - varchar(50)
				@Parameter_Tag -- tag - varchar(100)
			)
		SET @Parameter_Id = SCOPE_IDENTITY()

	END
	ELSE
		
	BEGIN
		UPDATE dbo.TB_PARAMETERS_INFO	
			SET Description = @Parameter_Description,
				DataType = @Parameter_Type,
				Unit = @Parameter_Unit,
				Tag = @Parameter_Tag
			WHERE Id_Parameter_Info = @ID
		SET @Parameter_Id = @ID
	END   
END
GO
/****** Object:  StoredProcedure [dbo].[stp_add_Part]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_add_Part]
	-- Add the parameters for the stored procedure here
		@Part_Number VARCHAR(100),
		@Part_Description VARCHAR(200),
		@Part_ToolSet VARCHAR(100)
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)
	IF(@ID IS NULL)
	BEGIN
    
		INSERT dbo.TB_PARTS
			(
				PartNumber,
				Description,
				ToolSet
			)
		VALUES
			(
				@Part_Number, -- PartNumber - varchar(100)
				@Part_Description,  -- Description - varchar(200)
				@Part_ToolSet -- ToolSet - varchar(100)
			)
	END
    ELSE
	BEGIN
		UPDATE dbo.TB_PARTS
		SET Description = @Part_Description,
		    ToolSet = @Part_ToolSet 
		WHERE Id_Parts = @ID
	END
	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_add_Result]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_add_Result]
	-- Add the parameters for the stored procedure here
		@Result_Description VARCHAR(100),
		@Result_Type VARCHAR(100),
		@Result_Unit VARCHAR(50),
		@Result_Tag VARCHAR(100),
		@Result_Hidden bit,
		@Result_Id BIGINT OUTPUT
AS
BEGIN
    
	DECLARE @ID BIGINT

	SET @ID =(SELECT TOP(1)Id_Result_Info FROM dbo.TB_RESULTS_INFO WHERE tag= @Result_tag)
	if (@ID IS NULL)
	BEGIN
		SET @ID =(SELECT TOP(1)Id_Result_Info FROM dbo.TB_RESULTS_INFO WHERE  Id_Result_Info= @Result_Id )
	END

	IF(@ID IS NULL)
	BEGIN
		INSERT dbo.TB_RESULTS_INFO
		    (
		        Description,
		        DataType,
				Unit,
				Hidden,
				Tag
		    )
		VALUES
		    (
		        @Result_Description, -- Description - varchar(200)
		        @Result_Type,  -- DataType - varchar(100)
				@Result_Unit, -- Unit - varchar(50)
				@Result_Hidden, -- Hidden - bit
				@Result_Tag -- Tag - varchar(100)
		    )
		SET @Result_Id = SCOPE_IDENTITY ()
	END
    ELSE
		
	BEGIN
		UPDATE dbo.TB_RESULTS_INFO	
			SET Description = @Result_Description,
                DataType = @Result_Type,
				Unit = @Result_Unit,
				Hidden = @Result_Hidden,
				Tag = @Result_Tag
			WHERE Id_Result_Info = @ID
		SET @Result_Id = @ID
	END
	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_add_Scrap]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_add_Scrap]
	-- Add the parameters for the stored procedure here
		@Scrap_Description VARCHAR(100),
		@Scrap_Code INT,
		@Scrap_Station BIGINT,
		@Scrap_Id BIGINT OUTPUT

AS
BEGIN

	DECLARE @ID BIGINT
	SET @ID =(SELECT TOP(1)Id_ScrapCode_Info FROM dbo.TB_SCRAPCODES_INFO WHERE TB_SCRAPCODES_INFO.Fk_Stations = @Scrap_Station AND TB_SCRAPCODES_INFO.ScrapCode = @Scrap_Code)

	IF(@ID IS NULL)
	BEGIN
		INSERT dbo.TB_SCRAPCODES_INFO
			(
				Description,
				ScrapCode,
				Fk_Stations
			)
		VALUES
			(
				@Scrap_Description, -- Description - varchar(100)
				@Scrap_Code, -- ScrapCode - int
				@Scrap_Station -- Fk_Stations - bigint
			)
		SET @Scrap_Id = SCOPE_IDENTITY()

	END
	ELSE
		
	BEGIN
		UPDATE dbo.TB_SCRAPCODES_INFO	
			SET Description = @Scrap_Description,
				ScrapCode = @Scrap_Code,
				Fk_Stations = @Scrap_Station
			WHERE Id_ScrapCode_Info = @ID
		SET @Scrap_Id = @ID
	END   
END
GO
/****** Object:  StoredProcedure [dbo].[stp_add_User]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_add_User]
	-- Add the parameters for the stored procedure here
		@User_Name VARCHAR(50),
		@User_Password VARCHAR(50),
		@User_Level TINYINT
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)id FROM dbo.TB_USERS WHERE name  = @User_Name)
	IF(@ID IS NULL)
	BEGIN
    
		INSERT dbo.TB_USERS
			(
				name,
				password, 
				userLevel
			)
		VALUES
			(
				@User_Name, -- name - varchar(50)
				@User_Password,  -- password - varchar(50)
				@User_Level  -- userLevel - tinyint
			)
	END
    ELSE
	BEGIN
		UPDATE dbo.TB_USERS
		SET password = @User_Password,
		    userLevel = @User_Level
		WHERE id = @ID
	END
	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Clear_Pieces]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Clear_Pieces]
	-- Add the parameters for the stored procedure here
		@Vehicle_Tag VARCHAR(50)
AS
BEGIN

	UPDATE dbo.TB_PIECES 
	SET TB_PIECES.vehicleTag = ''
	WHERE TB_PIECES.vehicleTag  = @Vehicle_Tag 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_delete_old_data]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_delete_old_data]
	-- Add the parameters for the stored procedure here
		@Date_To DATETIME

AS
BEGIN
    DECLARE @DT_PIECES TABLE (id BIGINT)
	INSERT @DT_PIECES 
	SELECT TB_PIECES.id FROM dbo.TB_PIECES WHERE TB_PIECES.LastUpdate < @Date_To

	DELETE FROM TB_RESULTS WHERE TB_RESULTS.Fk_Pieces IN (SELECT id FROM @DT_PIECES)
	DELETE FROM TB_JOBS WHERE TB_JOBS.Fk_Pieces IN (SELECT id FROM @DT_PIECES)
	DELETE FROM TB_PIECES WHERE TB_PIECES.LastUpdate < @Date_To

END
GO
/****** Object:  StoredProcedure [dbo].[stp_delete_Part]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_delete_Part]
	-- Add the parameters for the stored procedure here
		@Part_Number VARCHAR(100)
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)
	IF(@ID IS NOT NULL)
	BEGIN
		DELETE FROM TB_PARAMETERS_VALUES WHERE TB_PARAMETERS_VALUES.Fk_Parts = @ID
		DELETE FROM TB_PARTS WHERE TB_PARTS.Id_Parts = @ID    
	END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_delete_User]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_delete_User]
	-- Add the parameters for the stored procedure here
		@User_Name VARCHAR(50)
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)id FROM dbo.TB_USERS WHERE name  = @User_Name)
	IF(@ID IS NOT NULL)
	BEGIN
		DELETE FROM TB_USERS WHERE TB_USERS.id = @ID    
	END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Jobs]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_Get_Jobs]
		@Serial_Number VARCHAR(100)
AS
BEGIN

	SELECT TB_STATIONS_CONFIG.id as StationId, TB_STATIONS_CONFIG.description as stationDescription, TB_STATIONS_CONFIG.stationOrder as stationOrder, TB_MODULES_CONFIG.moduleOrder as moduleOrder,TB_MODULES_CONFIG.moduleName as moduleName, TB_JOBS.JobTime as JobTime, TB_JOBS.CycleTime as CycleTime, TB_JOBS.TimeStamp as TimeStamp, TB_JOBS.excluded as excluded FROM TB_JOBS 
	LEFT JOIN TB_PIECES on TB_PIECES.id = TB_JOBS.Fk_Pieces 
	LEFT JOIN TB_STATIONS_CONFIG on TB_STATIONS_CONFIG.id = TB_JOBS.Fk_Stations
	LEFT JOIN TB_MODULES_CONFIG on TB_MODULES_CONFIG.id = TB_STATIONS_CONFIG.Fk_Modules
	WHERE TB_PIECES.serialNumber = @Serial_Number
	ORDER BY TB_MODULES_CONFIG.moduleOrder, TB_STATIONS_CONFIG.stationOrder
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Last_JobTimes]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Last_JobTimes]
	-- Add the parameters for the stored procedure here
		@Station_Id BIGINT,
		@Max_Samples_Count INT
AS
BEGIN

	SELECT TOP(@Max_Samples_Count) TB_JOBS.JobTime, TB_JOBS.CycleTime
	FROM TB_JOBS
	WHERE TB_JOBS.Fk_Stations = @Station_Id AND TB_JOBS.excluded = 0
	ORDER BY TB_JOBS.id DESC
END
GO
/****** Object:  StoredProcedure [dbo].[stp_get_Last_RejectsRatio]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_get_Last_RejectsRatio]
	-- Add the parameters for the stored procedure here
		@Part_Number VARCHAR(100),
		@Max_Pieces_Count INT

AS
BEGIN

SELECT COUNT(*) as piecesCount, tb.pieceState as pieceState
FROM (
SELECT TOP(@Max_Pieces_Count) TB_PIECES.state as pieceState FROM TB_PIECES 
LEFT JOIN TB_LOTS ON TB_LOTS.id = TB_PIECES.Fk_Lots 
LEFT JOIN TB_PARTS ON TB_PARTS.Id_Parts = TB_LOTS.Fk_Parts
WHERE (@Part_Number IS NULL OR @Part_Number = '' OR TB_PARTS.PartNumber = @Part_Number) and TB_PIECES.UnloadDate IS NOT NULL ORDER BY TB_PIECES.UnloadDate DESC) tb
GROUP BY tb.pieceState 

END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Last_Results]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Last_Results]
	-- Add the parameters for the stored procedure here
		@Result_Id BIGINT,
		@Max_Samples_Count INT
AS
BEGIN

	SELECT TOP(@Max_Samples_Count) TB_RESULTS.value 
	FROM TB_RESULTS
	WHERE TB_RESULTS.Fk_Results_Info = @Result_Id 
	ORDER BY TB_RESULTS.id DESC
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Last_Results_ByStation]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Last_Results_ByStation]
	-- Add the parameters for the stored procedure here
		@Result_Id BIGINT,
		@Station_Id BIGINT,
		@Max_Samples_Count INT
AS
BEGIN

	SELECT Fk_Pieces as Fk_Pieces, Value as Value
	FROM TB_RESULTS 
	WHERE Fk_Pieces IN 
		(SELECT TOP(@Max_Samples_Count) Fk_Pieces FROM TB_JOBS WHERE Fk_Stations = @Station_Id ORDER BY TimeStamp desc) 
		AND Fk_Results_Info = @Result_Id
    ORDER BY TB_RESULTS.id DESC 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_get_Lot]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_get_Lot]
	-- Add the parameters for the stored procedure here
		@Lot_Id BIGINT,
		@Lot_Number VARCHAR(100) OUT,
		@Lot_StartDate DATETIME OUT,
		@Part_Number VARCHAR(100) OUT,
		@Part_ToolSet VARCHAR(100) OUT
AS
BEGIN

	SELECT @Lot_Number = TB_LOTS.LotNumber,
		   @Lot_StartDate = TB_LOTS.StartDate,
		   @Part_Number = TB_PARTS.PartNumber,
		   @Part_ToolSet = TB_PARTS.ToolSet 
	FROM TB_LOTS
	LEFT JOIN TB_PARTS ON TB_PARTS.Id_Parts = TB_LOTS.Fk_Parts 
	WHERE TB_LOTS.id = @Lot_Id
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Machine_Parameter_Value]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Machine_Parameter_Value]
	-- Add the parameters for the stored procedure here
		@Parameter_Id BIGINT,
		@Parameter_Value VARCHAR(100) OUT

AS
BEGIN
	DECLARE @ID BIGINT
	SET @ID =(SELECT TOP(1) id FROM dbo.TB_MACHINE_PARAMETERS_VALUES  WHERE @Parameter_Id = TB_MACHINE_PARAMETERS_VALUES.Id )
	if (@ID IS NULL)
	BEGIN
		SET @Parameter_Value = NULL
	END
	ELSE
	BEGIN
		SET @Parameter_Value = (SELECT TOP(1) value FROM dbo.TB_MACHINE_PARAMETERS_VALUES WHERE id=@ID)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Module_Infos]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Module_Infos]
	-- Add the parameters for the stored procedure here
		@Module_Name VARCHAR(255),
		@Module_ID BIGINT OUT,
		@Module_Description VARCHAR(255) OUT
AS
BEGIN


	SELECT TOP(1)@Module_ID = TB_MODULES_CONFIG.id, @Module_Description=TB_MODULES_CONFIG.moduleDescription
	FROM TB_MODULES_CONFIG
	WHERE TB_MODULES_CONFIG.moduleName = @Module_Name	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Module_IsExcluded]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Module_IsExcluded]
	-- Add the parameters for the stored procedure here
		@Module_Id BIGINT,
		@IsExcluded BIT OUT
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)id FROM dbo.TB_MODULES_CONFIG WHERE id = @Module_Id)

	IF(@ID IS NULL)
		BEGIN
			SET @IsExcluded = 0
		END
	ELSE
		BEGIN
			SELECT TOP(1) @IsExcluded = TB_MODULES_STATUS.Excluded FROM dbo.TB_MODULES_STATUS WHERE Fk_Modules = @Module_Id

			IF(@IsExcluded IS NULL)
			BEGIN
				SET @IsExcluded = 0
			END
		END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Modules]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Modules]
AS
BEGIN
	 SELECT TB_MODULES_CONFIG.id,
			TB_MODULES_CONFIG.moduleOrder,
			TB_MODULES_CONFIG.moduleName,
			TB_MODULES_CONFIG.moduleDescription,
			TB_MODULES_CONFIG.canBeExcluded,
			TB_MODULES_CONFIG.ipAddress
	 FROM TB_MODULES_CONFIG
	 WHERE TB_MODULES_CONFIG.hidden =0
	 ORDER BY TB_MODULES_CONFIG.moduleOrder  
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_New_VehicleInstance]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_New_VehicleInstance]
	-- Add the parameters for the stored procedure here
		@Vehicle_Id SMALLINT,
		@Vehicle_Instance TINYINT OUT
AS
BEGIN
    DECLARE @ID AS BIGINT

	SELECT TOP(1) @ID = TB_VEHICLE_INSTANCES.Id, @Vehicle_Instance = TB_VEHICLE_INSTANCES.vehicleInstance FROM dbo.TB_VEHICLE_INSTANCES WHERE TB_VEHICLE_INSTANCES.vehicleId = @Vehicle_Id

	IF(@ID IS NULL)
		BEGIN

			SET @Vehicle_Instance = 1
			INSERT dbo.TB_VEHICLE_INSTANCES
				(
					vehicleId,
					vehicleInstance 
				)
			VALUES
				(
					@Vehicle_Id, -- vehicleId - SMALLINT
					@Vehicle_Instance -- tinyint
				)
		END
    ELSE
		BEGIN
			if (@Vehicle_Instance = 99)
				BEGIN
					SET @Vehicle_Instance = 1	
				END
			ELSE
				BEGIN
					SET @Vehicle_Instance = @Vehicle_Instance + 1
				END

			UPDATE dbo.TB_VEHICLE_INSTANCES
			SET vehicleInstance = @Vehicle_Instance
			WHERE Id = @ID
		END
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_NextPaths]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_NextPaths]
AS
BEGIN
	 SELECT TB_NEXTPATHS_CONFIG.pathId,
	        TB_NEXTPATHS_CONFIG.nextpathId      
	 FROM TB_NEXTPATHS_CONFIG
END
GO
/****** Object:  StoredProcedure [dbo].[stp_get_Parameter]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_get_Parameter]
	-- Add the parameters for the stored procedure here
		@Parameter_Tag VARCHAR(100),
		@Parameter_Id BIGINT OUTPUT

AS
BEGIN

	SET @Parameter_Id =(SELECT TOP(1)Id_Parameter_Info FROM dbo.TB_PARAMETERS_INFO WHERE TB_PARAMETERS_INFO.Tag= @Parameter_Tag)
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_ParameterByTag_Value]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_ParameterByTag_Value]
	-- Add the parameters for the stored procedure here
		@Part_Number VARCHAR(100),
		@Parameter_Tag VARCHAR(100),
		@Parameter_Value VARCHAR(100) OUT

AS
BEGIN
    DECLARE @ID_PART AS BIGINT
	SET @ID_PART =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)

	if (@ID_PART IS NOT NULL)
	BEGIN
		DECLARE @ID_PARAMETER AS BIGINT
		SET @ID_PARAMETER =(SELECT TOP(1)Id_Parameter_Info FROM dbo.TB_PARAMETERS_INFO WHERE Tag = @Parameter_Tag)

		if (@ID_PARAMETER IS NOT NULL)
		BEGIN
			SET @Parameter_Value =(SELECT TOP(1)Value FROM dbo.TB_PARAMETERS_VALUES WHERE Fk_Parameters_Info = @ID_PARAMETER AND Fk_Parts  = @ID_PART)
		END	
	END
     
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Parameters]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_Get_Parameters]
		@Part_Number VARCHAR(100)
AS
BEGIN
	
	DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)



	SELECT TB_PARAMETERS_INFO.Description, TB_PARAMETERS_INFO.DataType, TB_PARAMETERS_INFO.Id_Parameter_Info, TB_PARAMETERS_INFO.Unit, TB_PARAMETERS_VALUES.Value
	FROM TB_PARAMETERS_INFO
	full outer JOIN TB_PARAMETERS_VALUES ON TB_PARAMETERS_VALUES.Fk_Parameters_Info=TB_PARAMETERS_INFO.Id_Parameter_Info 
--	full outer JOIN TB_PARTS ON TB_PARAMETERS_VALUES.Fk_Parts = TB_PARTS.Id_Parts 
	WHERE TB_PARAMETERS_VALUES.Fk_Parts = @ID 
	union
	SELECT TB_PARAMETERS_INFO.Description, TB_PARAMETERS_INFO.DataType, TB_PARAMETERS_INFO.Id_Parameter_Info, TB_PARAMETERS_INFO.Unit, null
	FROM TB_PARAMETERS_INFO where TB_PARAMETERS_INFO.Id_Parameter_Info not in(
	SELECT  TB_PARAMETERS_INFO.Id_Parameter_Info
	FROM TB_PARAMETERS_INFO
	full outer JOIN TB_PARAMETERS_VALUES ON TB_PARAMETERS_VALUES.Fk_Parameters_Info=TB_PARAMETERS_INFO.Id_Parameter_Info 
	WHERE TB_PARAMETERS_VALUES.Fk_Parts = @ID)
	ORDER BY TB_PARAMETERS_INFO.Description
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Parameters_Values]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Parameters_Values]
	-- Add the parameters for the stored procedure here
		@Part_Number VARCHAR(100),
		@Parameter_Id_Vett VARCHAR(200)		
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)Id_Parameter_Info FROM dbo.TB_PARAMETERS_INFO WHERE  Id_Parameter_Info= @id )
	IF(@ID IS NULL)
	BEGIN	
	SELECT * FROM V000_Parameter_Values
		WHERE  [Id_Parameter_Info]  IN (SELECT convert(int, item) FROM dbo.splitstring(@Parameter_Id_Vett))
		  AND [PartNumber] = @Part_Number 
	END
     
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Parts]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_Get_Parts]
AS
BEGIN

	SELECT TB_PARTS.Id_Parts,TB_PARTS.PartNumber,TB_PARTS.Description,TB_PARTS.ToolSet  FROM TB_PARTS ORDER BY TB_PARTS.PartNumber
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Paths]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Paths]
AS
BEGIN
	 SELECT TB_PATHS_CONFIG.id,
			TB_PATHS_CONFIG.pathId,
			TB_PATHS_CONFIG.Fk_Modules
	 FROM TB_PATHS_CONFIG
	 ORDER BY TB_PATHS_CONFIG.pathId 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Piece_ScrapCode]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_Get_Piece_ScrapCode]
		@Serial_Number VARCHAR(50),
		@ScrapCode_Id BIGINT OUT,
		@RejectModule_Id BIGINT OUT,
		@RejectStation_Id BIGINT OUT,
		@ScrapCode INT OUT
AS
BEGIN

	SELECT TOP(1) @ScrapCode_Id = TB_SCRAPCODES_INFO.Id_ScrapCode_Info,@ScrapCode = TB_SCRAPCODES_INFO.scrapCode, @RejectModule_Id = TB_STATIONS_CONFIG.Fk_Modules, @RejectStation_Id = TB_SCRAPCODES_INFO.Fk_Stations FROM TB_PIECES
	LEFT JOIN TB_SCRAPCODES_INFO ON TB_SCRAPCODES_INFO.Id_ScrapCode_Info = TB_PIECES.Fk_ScrapCodes_Info
	LEFT JOIN TB_STATIONS_CONFIG ON TB_STATIONS_CONFIG.id = TB_SCRAPCODES_INFO.Fk_Stations
	WHERE TB_PIECES.serialNumber =  @Serial_Number
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Piece_State]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Piece_State]
	-- Add the parameters for the stored procedure here
		@Serial_Number VARCHAR(50),
		@Vehicle_Tag VARCHAR(50) OUT,
		@Part_Number VARCHAR(100) OUT,
		@Lot_Number VARCHAR(100) OUT,
		@LastWorkedStation_Id BIGINT OUT,
		@Piece_State TINYINT OUT,
		@Piece_Orientation TINYINT OUT,
		@Piece_Id BIGINT OUT,
		@Piece_ScrapCode INT OUT
AS
BEGIN

	SELECT @Vehicle_Tag = TB_PIECES.vehicleTag,
		   @Part_Number = TB_PARTS.PartNumber,
		   @Lot_Number = TB_LOTS.LotNumber,
		   @LastWorkedStation_Id = TB_PIECES.lastWorkedStation,
		   @Piece_State = TB_PIECES.state,
		   @Piece_Orientation = TB_PIECES.orientation,
		   @Piece_ScrapCode = TB_SCRAPCODES_INFO.ScrapCode, 
		   @Piece_Id = TB_PIECES.id 
	FROM TB_PIECES 
	LEFT JOIN TB_LOTS ON TB_LOTS.id =TB_PIECES.Fk_Lots
	LEFT JOIN TB_PARTS ON TB_PARTS.Id_Parts=TB_LOTS.Fk_Parts
	LEFT JOIN TB_SCRAPCODES_INFO ON TB_SCRAPCODES_INFO.Id_ScrapCode_Info = TB_PIECES.Fk_ScrapCodes_Info 
	WHERE serialNumber = @Serial_Number 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Piece_State2]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Piece_State2]
	-- Add the parameters for the stored procedure here
		@Vehicle_Tag VARCHAR(50),
		@Serial_Number VARCHAR(50) OUT,
		@Part_Number VARCHAR(100) OUT,
		@Lot_Number VARCHAR(100) OUT,
		@LastWorkedStation_Id BIGINT OUT,
		@Piece_State TINYINT OUT,
		@Piece_Orientation TINYINT OUT,
		@Piece_Id BIGINT OUT,
	    @Piece_ScrapCode INT OUT
AS
BEGIN

	SELECT @Serial_Number = TB_PIECES.serialNumber,
		   @Part_Number = TB_PARTS.PartNumber,
		   @Lot_Number = TB_LOTS.LotNumber,
		   @LastWorkedStation_Id = TB_PIECES.lastWorkedStation,
		   @Piece_State = TB_PIECES.state,
		   @Piece_Orientation = TB_PIECES.orientation,
		   @Piece_ScrapCode = TB_SCRAPCODES_INFO.ScrapCode, 
		   @Piece_Id = TB_PIECES.id 
	FROM TB_PIECES 
	LEFT JOIN TB_LOTS ON TB_LOTS.id=TB_PIECES.Fk_Lots
	LEFT JOIN TB_PARTS ON TB_PARTS.Id_Parts=TB_LOTS.Fk_Parts
	LEFT JOIN TB_SCRAPCODES_INFO ON TB_SCRAPCODES_INFO.Id_ScrapCode_Info = TB_PIECES.Fk_ScrapCodes_Info 
	WHERE vehicleTag  = @Vehicle_Tag 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Pieces]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_Get_Pieces]
		@Date_From DATETIME,
		@Date_To DATETIME,
		@Part_Number VARCHAR(100),
		@Lot_Number VARCHAR(100),
		@Vehicle_Tag VARCHAR(50),
		@Piece_State TINYINT,
		@RejectStation_Id BIGINT,
		@MaxRows INT
AS
BEGIN

	SELECT TOP (@MaxRows) TB_PIECES.serialNumber, TB_PARTS.PartNumber, TB_LOTS.LotNumber, TB_PIECES.LoadDate, TB_PIECES.UnloadDate, TB_PIECES.state, TB_PIECES.loadVehicleTag, TB_SCRAPCODES_INFO.scrapCode, TB_SCRAPCODES_INFO.Description AS scrapDescription, TB_MODULES_CONFIG.moduleName AS rejectStationModuleName, TB_STATIONS_CONFIG.stationOrder  AS rejectStationOrder, TB_STATIONS_CONFIG.description AS rejectStation FROM TB_PIECES
	LEFT JOIN TB_SCRAPCODES_INFO ON TB_SCRAPCODES_INFO.Id_ScrapCode_Info = TB_PIECES.Fk_ScrapCodes_Info
	LEFT JOIN TB_LOTS ON TB_LOTS.Id = TB_PIECES.Fk_Lots
	LEFT JOIN TB_PARTS ON TB_PARTS.Id_Parts = TB_LOTS.Fk_Parts
	LEFT JOIN TB_STATIONS_CONFIG ON TB_STATIONS_CONFIG.id = TB_SCRAPCODES_INFO.Fk_Stations 
	LEFT JOIN TB_PATHS_CONFIG ON TB_PATHS_CONFIG.id  = TB_STATIONS_CONFIG.Fk_Paths  
	LEFT JOIN TB_MODULES_CONFIG ON TB_MODULES_CONFIG.id = TB_STATIONS_CONFIG.Fk_Modules 
	WHERE (@Part_Number IS NULL OR @Part_Number = '' OR TB_PARTS.PartNumber = @Part_Number) AND
	      (@Lot_Number IS NULL OR @Lot_Number = '' OR TB_LOTS.LotNumber = @Lot_Number) AND
	      (@Vehicle_Tag IS NULL OR @Vehicle_Tag = '' OR TB_PIECES.loadVehicleTag = @Vehicle_Tag) AND
	      (@Piece_State = 0 OR TB_PIECES.state = @Piece_State) AND
	      (@RejectStation_Id = 0 OR TB_SCRAPCODES_INFO.Fk_Stations = @RejectStation_Id) AND
	      ((TB_PIECES.LoadDate >= @Date_From AND TB_PIECES.LoadDate <= @Date_To AND TB_PIECES.UnloadDate IS NULL) OR (TB_PIECES.LoadDate <= @Date_To AND TB_PIECES.UnloadDate >= @Date_From))
	ORDER BY TB_PIECES.id 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_PiecesFifo]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_PiecesFifo]
	@MaxRows INT

AS
BEGIN

	SELECT TOP (@MaxRows) TB_PIECES.serialNumber FROM TB_PIECES
	WHERE TB_PIECES.UnloadDate IS NULL AND TB_PIECES.state = 1
	ORDER BY TB_PIECES.id

END
GO
/****** Object:  StoredProcedure [dbo].[stp_get_Production_Rate]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_get_Production_Rate]
	-- Add the parameters for the stored procedure here
		@Last_Minutes INT,
		@Pieces_Count INT OUTPUT,
		@Range INT OUTPUT

AS
BEGIN

DECLARE @UnloadDateMin DATETIME
DECLARE @UnloadDateMax DATETIME
SELECT @UnloadDateMin = MIN(UnloadDate), @UnloadDateMax = MAX(UnloadDate), @Pieces_Count=COUNT(*) FROM TB_PIECES WHERE UnloadDate > DATEADD(minute, -@Last_Minutes, GetDATE()) and state = 1

SET @Range = DATEDIFF(MILLISECOND,  @UnloadDateMin, @UnloadDateMax);
END

GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Requirements]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Requirements]
AS
BEGIN
	 SELECT TB_REQUIREMENTS_CONFIG.id,
	        TB_REQUIREMENTS_CONFIG.stationId,
	        TB_REQUIREMENTS_CONFIG.reqStationId,
	        TB_REQUIREMENTS_CONFIG.reqState,
	        TB_REQUIREMENTS_CONFIG.reqOrientation
	 FROM TB_REQUIREMENTS_CONFIG
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Result_Value]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_Get_Result_Value]
		@Serial_Number VARCHAR(50),
		@Result_Id BIGINT,
		@Result_Value VARCHAR(200) OUTPUT
AS
BEGIN
	DECLARE @ID_PIECES BIGINT
	SET @ID_PIECES =(SELECT TOP(1) id FROM dbo.TB_PIECES  WHERE  serialNumber = @Serial_Number )
	if (@ID_PIECES IS NULL)
	BEGIN
		SET @Result_Value = NULL
	END
	ELSE
	BEGIN
		SET @Result_Value = (SELECT TOP(1) TB_RESULTS.value FROM dbo.TB_RESULTS WHERE TB_RESULTS.Fk_Pieces=@ID_PIECES AND TB_RESULTS.Fk_Results_Info =@Result_Id)
	END

END
GO
/****** Object:  StoredProcedure [dbo].[stp_get_ResultId]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_get_ResultId]
	-- Add the parameters for the stored procedure here
		@Result_Tag VARCHAR(100),
		@Result_Id BIGINT OUTPUT
AS
BEGIN    
	SET @Result_Id =(SELECT TOP(1)Id_Result_Info FROM dbo.TB_RESULTS_INFO WHERE tag= @Result_tag)	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Results]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_Get_Results]
		@Serial_Number VARCHAR(100)
AS
BEGIN

	SELECT TB_RESULTS.Fk_Results_Info, TB_RESULTS_INFO.Description,TB_RESULTS_INFO.DataType,TB_RESULTS_INFO.Unit, TB_RESULTS.value, TB_STATIONS_CONFIG.id as StationId, TB_STATIONS_CONFIG.description as stationDescription, TB_STATIONS_CONFIG.stationOrder as stationOrder, TB_MODULES_CONFIG.moduleName as moduleName, TB_JOBS.JobTime as JobTime, TB_JOBS.TimeStamp as TimeStamp FROM TB_RESULTS 
	LEFT JOIN TB_PIECES on TB_PIECES.id = TB_RESULTS.Fk_Pieces 
	LEFT JOIN TB_RESULTS_INFO ON TB_RESULTS_INFO.Id_Result_Info = TB_RESULTS.Fk_Results_Info  
	LEFT JOIN TB_JOBS on TB_JOBS.id = TB_RESULTS.Fk_Jobs
	LEFT JOIN TB_STATIONS_CONFIG on TB_STATIONS_CONFIG.id = TB_JOBS.Fk_Stations
	LEFT JOIN TB_MODULES_CONFIG on TB_MODULES_CONFIG.id = TB_STATIONS_CONFIG.Fk_Modules
	WHERE TB_PIECES.serialNumber = @Serial_Number AND TB_RESULTS_INFO.Hidden = 0
	ORDER BY TB_MODULES_CONFIG.moduleOrder, TB_STATIONS_CONFIG.stationOrder, TB_RESULTS_INFO.Description
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Results_Values]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Results_Values]
	-- Add the parameters for the stored procedure here
		@Serial_Number VARCHAR(100),
		@Result_Id_Vett VARCHAR(200)		
AS
BEGIN
    DECLARE @ID_PIECES AS BIGINT
	SET @ID_PIECES =(SELECT TOP(1) id FROM dbo.TB_PIECES  WHERE  serialNumber = @Serial_Number )
	IF(@ID_PIECES IS NOT NULL)
	BEGIN	
	SELECT * FROM V001_Result_Values
		WHERE  [Id_Result_Info]  IN (SELECT convert(int, item) FROM dbo.splitstring(@Result_Id_Vett))
		  AND [SerialNumber] = @Serial_Number 
	END
     
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_ScrapCodes]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_ScrapCodes]
AS
BEGIN

	SELECT TB_SCRAPCODES_INFO.Id_ScrapCode_Info, TB_SCRAPCODES_INFO.Fk_Stations, TB_SCRAPCODES_INFO.Description, TB_SCRAPCODES_INFO.ScrapCode  
	FROM TB_SCRAPCODES_INFO
	ORDER BY TB_SCRAPCODES_INFO.description
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Station_IsExcluded]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Station_IsExcluded]
	-- Add the parameters for the stored procedure here
		@Station_Id BIGINT,
		@IsExcluded BIT OUT
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)id FROM dbo.TB_STATIONS_CONFIG WHERE id = @Station_Id)

	IF(@ID IS NULL)
		BEGIN
			SET @IsExcluded = 0
		END
	ELSE
		BEGIN
			SELECT TOP(1) @IsExcluded = TB_STATIONS_STATUS.Excluded FROM dbo.TB_STATIONS_STATUS WHERE Fk_Stations = @Station_Id

			IF(@IsExcluded IS NULL)
			BEGIN
				SET @IsExcluded = 0
			END
		END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Stations]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Stations]
AS
BEGIN
	 SELECT TB_STATIONS_CONFIG.id,
	        TB_STATIONS_CONFIG.description,
			TB_STATIONS_CONFIG.stationOrder,
			TB_STATIONS_CONFIG.Fk_Modules,
			TB_STATIONS_CONFIG.Fk_Paths,
			TB_STATIONS_CONFIG.position,
			TB_STATIONS_CONFIG.alwaysStop,
			TB_STATIONS_CONFIG.isLoad,
			TB_STATIONS_CONFIG.isUnload,
			TB_STATIONS_CONFIG.isVirtual,
			TB_STATIONS_CONFIG.isBackStation,
			TB_STATIONS_CONFIG.isReworkStation,
			TB_STATIONS_CONFIG.canBeExcluded,
			TB_STATIONS_CONFIG.hasTagReader,
			TB_STATIONS_CONFIG.isPreToolChanger,
			TB_STATIONS_CONFIG.isToolChanger,
			TB_STATIONS_CONFIG.isToolChecker,
			TB_STATIONS_CONFIG.isMaintenanceStation,
			TB_STATIONS_CONFIG.isCounterStation,
			TB_STATIONS_CONFIG.virtualJobTime, 
			TB_STATIONS_CONFIG.masterStationId, 
			TB_STATIONS_CONFIG.slaveindex,
			TB_STATIONS_CONFIG.groupMasterStationId, 
			TB_STATIONS_CONFIG.groupIndex,
			TB_STATIONS_CONFIG.RestVehicle,
			TB_STATIONS_CONFIG.traficStationId
	 FROM TB_STATIONS_CONFIG
	 LEFT JOIN TB_PATHS_CONFIG ON TB_PATHS_CONFIG.id = TB_STATIONS_CONFIG.Fk_Paths
	 LEFT JOIN TB_MODULES_CONFIG ON TB_MODULES_CONFIG.id = TB_STATIONS_CONFIG.Fk_Modules 
	 ORDER BY TB_MODULES_CONFIG.moduleOrder, TB_STATIONS_CONFIG.stationOrder
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Stations_Ids]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Stations_Ids]
AS
BEGIN

	SELECT TB_STATIONS_CONFIG.id, TB_MODULES_CONFIG.moduleName, TB_STATIONS_CONFIG.stationOrder, TB_STATIONS_CONFIG.description as stationDescription, TB_STATIONS_CONFIG.groupMasterStationId, TB_STATIONS_CONFIG.Fk_Modules 
	FROM TB_STATIONS_CONFIG
	LEFT JOIN TB_MODULES_CONFIG ON TB_MODULES_CONFIG.id  = TB_STATIONS_CONFIG.Fk_Modules 
	WHERE TB_STATIONS_CONFIG.isVirtual = 0 AND TB_STATIONS_CONFIG.masterStationId = 0
	ORDER BY TB_MODULES_CONFIG.moduleOrder, TB_STATIONS_CONFIG.stationOrder, TB_STATIONS_CONFIG.description
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Stations_Infos]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Stations_Infos]
	-- Add the parameters for the stored procedure here
		@Module_Name VARCHAR(255)
AS
BEGIN

	SELECT TB_STATIONS_CONFIG.id, TB_STATIONS_CONFIG.stationOrder, TB_STATIONS_CONFIG.description
	FROM TB_STATIONS_CONFIG
	LEFT JOIN TB_MODULES_CONFIG ON TB_MODULES_CONFIG.id  = TB_STATIONS_CONFIG.Fk_Modules 
	WHERE TB_STATIONS_CONFIG.isVirtual = 0 AND TB_STATIONS_CONFIG.masterStationId = 0
	AND TB_MODULES_CONFIG.moduleName = @Module_Name 
	ORDER BY TB_MODULES_CONFIG.moduleName, TB_STATIONS_CONFIG.stationOrder, TB_STATIONS_CONFIG.description  
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Users]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_Get_Users]
AS
BEGIN

	SELECT TB_USERS.id, TB_USERS.name, TB_USERS.password,TB_USERS.userLevel FROM TB_USERS
END
GO
/****** Object:  StoredProcedure [dbo].[stp_get_Vehicle_Last_PieceStates]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_get_Vehicle_Last_PieceStates]
	-- Add the parameters for the stored procedure here
		@Vehicle_Tag VARCHAR(50),
		@Max_Pieces_Count INT

AS
BEGIN

SELECT TOP(@Max_Pieces_Count) TB_PIECES.state as pieceState FROM TB_PIECES WHERE TB_PIECES.loadVehicleTag = @Vehicle_Tag and TB_PIECES.UnloadDate IS NOT NULL ORDER BY TB_PIECES.UnloadDate DESC

END
GO
/****** Object:  StoredProcedure [dbo].[stp_Get_Vehicle_State]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Get_Vehicle_State]
	-- Add the parameters for the stored procedure here
		@Vehicle_Tag VARCHAR(50),
		@IsExcluded BIT OUT,
		@NeedMaintenance BIT OUT,
		@Part_Number VARCHAR(100) OUT,
		@Part_ToolSet VARCHAR(100) OUT,
		@ForceGotoStationId BIGINT OUT
AS
BEGIN
    DECLARE @ID_PARTS AS BIGINT

	SELECT TOP(1)@IsExcluded = TB_VEHICLES_STATUS.excluded, @NeedMaintenance = TB_VEHICLES_STATUS.NeedMaintenance, @ID_PARTS = TB_VEHICLES_STATUS.Fk_Parts, @ForceGotoStationId = TB_VEHICLES_STATUS.ForceGoToStationId
	FROM TB_VEHICLES_STATUS
	WHERE TB_VEHICLES_STATUS.tag = @Vehicle_Tag	 

	if (@ID_PARTS IS NOT NULL)
	BEGIN
		SELECT @Part_Number =PartNumber, @Part_ToolSet = ToolSet FROM dbo.TB_PARTS WHERE Id_Parts = @ID_PARTS
	END
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Load_Piece]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Load_Piece]
	-- Add the parameters for the stored procedure here
		@Serial_Number VARCHAR(50),
		@Part_Number VARCHAR(100),
		@Lot_Number VARCHAR(100),
		@Vehicle_Tag VARCHAR(50),
		@Piece_Id BIGINT OUT
AS
BEGIN
	
    DECLARE @ID_PARTS AS BIGINT
	SET @ID_PARTS =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)

	DECLARE @ID_LOTS AS BIGINT
	SET @ID_LOTS =(SELECT TOP(1)id FROM dbo.TB_LOTS WHERE LotNumber = @Lot_Number )

	IF(@ID_PARTS IS NULL OR @ID_LOTS IS NULL)
		BEGIN
			SET @Piece_Id = 0
		END
	ELSE
	BEGIN
		UPDATE dbo.TB_PIECES SET TB_PIECES.vehicleTag = '' WHERE TB_PIECES.vehicleTag = @Vehicle_Tag

		DECLARE @ID AS BIGINT
		SET @ID =(SELECT TOP(1)id FROM dbo.TB_PIECES WHERE serialNumber = @Serial_Number)

		IF(@ID IS NULL)
		BEGIN
			INSERT dbo.TB_PIECES
				(
					TB_PIECES.serialNumber, 
					TB_PIECES.Fk_Lots, 
					TB_PIECES.lastWorkedStation , 
					TB_PIECES.vehicleTag , 
					TB_PIECES.loadVehicleTag , 
					TB_PIECES.state, 
					TB_PIECES.orientation, 
					TB_PIECES.Fk_ScrapCodes_Info,
					TB_PIECES.LoadDate,
					TB_PIECES.LastUpdate
				)
			VALUES
				(
					@Serial_Number, -- serialNumber - varchar(50)
					@ID_LOTS, -- Fk_Lots - bigint 
					0,-- lastWorkedStation - bigint, 
					@Vehicle_Tag, -- vehicleTag - varchar(50)
					@Vehicle_Tag, -- loadVehicleTag - varchar(50)
					1, -- state - tinyint 
					0, -- orientation - tinyint 
					NULL, -- Fk_ScrapCodes_Info - bigint 
					GETDATE(),
					GETDATE()
				)
			SET @Piece_Id = SCOPE_IDENTITY()

		END
		ELSE
		BEGIN
			UPDATE dbo.TB_PIECES 
			SET TB_PIECES.vehicleTag = @Vehicle_Tag
			WHERE TB_PIECES.id = @ID

			SET @Piece_Id = @ID
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Rename_Part]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Rename_Part]
	-- Add the parameters for the stored procedure here
		@Part_Number VARCHAR(100),
		@New_Part_Number VARCHAR(100)
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)
	IF(@ID IS NOT NULL)
	BEGIN
	    DECLARE @ID_NEW AS BIGINT
		SET @ID_NEW =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @New_Part_Number)

		IF(@ID_NEW IS NULL)
		BEGIN
			UPDATE dbo.TB_PARTS 
			SET TB_PARTS.PartNumber = @New_Part_Number
			WHERE TB_PARTS.Id_Parts = @ID 			
		END
	END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Rename_User]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Rename_User]
	-- Add the parameters for the stored procedure here
		@User_Name VARCHAR(50),
		@New_User_Name VARCHAR(50)
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)id FROM dbo.TB_USERS WHERE name  = @User_Name)
	IF(@ID IS NOT NULL)
	BEGIN
	    DECLARE @ID_NEW AS BIGINT
		SET @ID_NEW =(SELECT TOP(1)id FROM dbo.TB_USERS WHERE name = @New_User_Name)

		IF(@ID_NEW IS NULL)
		BEGIN
			UPDATE dbo.TB_USERS 
			SET TB_USERS.name = @New_User_Name
			WHERE TB_USERS.id = @ID 			
		END
	END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_Machine_Parameter_Value]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_Machine_Parameter_Value]
	-- Add the parameters for the stored procedure here
		@Parameter_Id BIGINT,
		@Parameter_Value VARCHAR(100)

AS
BEGIN
	DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)Id FROM dbo.TB_MACHINE_PARAMETERS_VALUES WHERE @Parameter_Id = TB_MACHINE_PARAMETERS_VALUES.Id)

		if (@ID IS NULL)
		BEGIN
			INSERT dbo.TB_MACHINE_PARAMETERS_VALUES
			(
				Id,
				Value
			)
			VALUES
			(
				@Parameter_Id,  -- Id - bigint
				@Parameter_Value  -- Value - varchar(100)
			)
		END
		ELSE
		BEGIN
			UPDATE dbo.TB_MACHINE_PARAMETERS_VALUES	
				SET Value = @Parameter_Value
				WHERE Id = @ID
		END		     
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_Module_IpAddress]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_Module_IpAddress]
	-- Add the parameters for the stored procedure here
		@Module_Name VARCHAR(255),
		@IpAddress VARCHAR(15)
AS
BEGIN

	UPDATE dbo.TB_MODULES_CONFIG
	SET TB_MODULES_CONFIG.ipAddress = @IpAddress
	WHERE TB_MODULES_CONFIG.moduleName = @Module_Name

	
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_Module_IsExcluded]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_Module_IsExcluded]
	-- Add the parameters for the stored procedure here
		@Module_Id BIGINT,
		@IsExcluded BIT
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)id FROM dbo.TB_MODULES_CONFIG WHERE id = @Module_Id)

	IF(@ID IS NOT NULL)
		BEGIN
			SET @ID = (SELECT TOP(1)TB_MODULES_STATUS.Fk_Modules FROM dbo.TB_MODULES_STATUS WHERE Fk_Modules = @Module_Id)

			IF(@ID IS NULL)
				INSERT TB_MODULES_STATUS
				(
					TB_MODULES_STATUS.Fk_Modules,
					TB_MODULES_STATUS.Excluded
				)
				VALUES
				(
					@Module_Id,
					@IsExcluded 
				)
			BEGIN
				UPDATE TB_MODULES_STATUS
				SET TB_MODULES_STATUS.Excluded = @IsExcluded
				WHERE Fk_Modules = @Module_Id
			END
		END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_ModuleDescription]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_ModuleDescription]
	-- Add the parameters for the stored procedure here
		@Module_Id BIGINT,
		@Description VARCHAR(255)

AS
BEGIN

	UPDATE TB_MODULES_CONFIG SET moduleDescription = @Description WHERE id = @Module_Id 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_Parameter_Value]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_Parameter_Value]
	-- Add the parameters for the stored procedure here
		@Part_Number VARCHAR(100),
		@Parameter_Id BIGINT,
		@Parameter_Value VARCHAR(100)

AS
BEGIN
    DECLARE @ID_PART AS BIGINT
	SET @ID_PART =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)

	if (@ID_PART IS NOT NULL)
	BEGIN
		DECLARE @ID AS BIGINT
		SET @ID =(SELECT TOP(1)Id FROM dbo.TB_PARAMETERS_VALUES WHERE Fk_Parameters_Info = @Parameter_Id AND Fk_Parts  = @ID_PART)

			if (@ID IS NULL)
			BEGIN
				INSERT dbo.TB_PARAMETERS_VALUES
				(
					Fk_Parameters_Info,
					Fk_Parts,
					Value
				)
				VALUES
				(
					@Parameter_Id,  -- Fk_Parameters_Info - bigint
					@ID_PART, -- Fk_Parts - bigint
					@Parameter_Value  -- Value - varchar(100)
				)
			END
			ELSE
			BEGIN
				UPDATE dbo.TB_PARAMETERS_VALUES	
					SET Value = @Parameter_Value
					WHERE Id = @ID
			END			
	END
     
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_ParameterByTag_Value]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_ParameterByTag_Value]
	-- Add the parameters for the stored procedure here
		@Part_Number VARCHAR(100),
		@Parameter_Tag VARCHAR(100),
		@Parameter_Value VARCHAR(100)

AS
BEGIN
    DECLARE @ID_PART AS BIGINT
	SET @ID_PART =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)

	if (@ID_PART IS NOT NULL)
	BEGIN
		DECLARE @ID_PARAMETER AS BIGINT
		SET @ID_PARAMETER =(SELECT TOP(1)Id_Parameter_Info FROM dbo.TB_PARAMETERS_INFO WHERE Tag = @Parameter_Tag)

		if (@ID_PARAMETER IS NOT NULL)
		BEGIN
				DECLARE @ID AS BIGINT
				SET @ID =(SELECT TOP(1)Id FROM dbo.TB_PARAMETERS_VALUES WHERE Fk_Parameters_Info = @ID_PARAMETER AND Fk_Parts  = @ID_PART)

				if (@ID IS NULL)
				BEGIN
					INSERT dbo.TB_PARAMETERS_VALUES
					(
						Fk_Parameters_Info,
						Fk_Parts,
						Value
					)
					VALUES
					(
						@ID_PARAMETER,  -- Fk_Parameters_Info - bigint
						@ID_PART, -- Fk_Parts - bigint
						@Parameter_Value  -- Value - varchar(100)
					)
				END
				ELSE
				BEGIN
					UPDATE dbo.TB_PARAMETERS_VALUES	
						SET Value = @Parameter_Value
						WHERE Id = @ID
				END		
		END
	
	END
     
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_Part_Data]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_Part_Data]
	-- Add the parameters for the stored procedure here
		@Part_Number VARCHAR(100),
		@Part_Description VARCHAR(200),
		@Part_ToolSet VARCHAR(100)
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)
	IF(@ID IS NOT NULL)
	BEGIN
		UPDATE dbo.TB_PARTS 
		SET TB_PARTS.Description = @Part_Description,
		    TB_PARTS.ToolSet = @Part_ToolSet 
		WHERE TB_PARTS.Id_Parts = @ID 
	END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_Part_Description]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_Part_Description]
	-- Add the parameters for the stored procedure here
		@Part_Number VARCHAR(100),
		@Part_Description VARCHAR(200)
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)
	IF(@ID IS NOT NULL)
	BEGIN
		UPDATE dbo.TB_PARTS 
		SET TB_PARTS.Description = @Part_Description 
		WHERE TB_PARTS.Id_Parts = @ID 
	END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_Piece_State]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_Piece_State]
	-- Add the parameters for the stored procedure here
		@Piece_Id BIGINT,
		@LastWorkedStation_Id BIGINT,
		@Piece_State TINYINT,
		@Piece_Orientation TINYINT,
		@Piece_ScrapCode INT
AS
BEGIN
	DECLARE @ID_SCRAPCODE BIGINT

	SELECT TOP(1)@ID_SCRAPCODE = TB_PIECES.Fk_ScrapCodes_Info FROM TB_PIECES WHERE TB_PIECES.id = @Piece_Id

	IF (@Piece_State = 1)
		BEGIN
			SET @ID_SCRAPCODE = NULL
		END
	ELSE 
		BEGIN
			IF (@LastWorkedStation_Id <>0 AND @Piece_ScrapCode <> 0) 
				BEGIN
					SET @ID_SCRAPCODE = (SELECT TOP(1) TB_SCRAPCODES_INFO.Id_ScrapCode_Info FROM TB_SCRAPCODES_INFO WHERE TB_SCRAPCODES_INFO.Fk_Stations=@LastWorkedStation_Id AND TB_SCRAPCODES_INFO.ScrapCode = @Piece_ScrapCode)

					if (@ID_SCRAPCODE IS NULL)
					BEGIN						
						INSERT TB_SCRAPCODES_INFO (Fk_Stations, scrapCode, Description ) VALUES (@LastWorkedStation_Id, @Piece_ScrapCode, @Piece_ScrapCode)
						SET @ID_SCRAPCODE = (SELECT TOP(1) TB_SCRAPCODES_INFO.Id_ScrapCode_Info FROM TB_SCRAPCODES_INFO WHERE TB_SCRAPCODES_INFO.Fk_Stations=@LastWorkedStation_Id AND TB_SCRAPCODES_INFO.ScrapCode = @Piece_ScrapCode)
					END
				END
		END
	
	UPDATE dbo.TB_PIECES 
	SET TB_PIECES.lastWorkedStation = @LastWorkedStation_Id,
		TB_PIECES.state = @Piece_State,
		TB_PIECES.orientation = @Piece_Orientation,
		TB_PIECES.Fk_ScrapCodes_Info = @ID_SCRAPCODE,
		TB_PIECES.LastUpdate = GetDATE()
	WHERE TB_PIECES.id = @Piece_Id 

	
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_Result_Value]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[stp_Set_Result_Value]
		@Serial_Number VARCHAR(100),
		@Result_Id BIGINT,
		@Result_Value VARCHAR(200)
AS
BEGIN
	DECLARE @ID_PIECES INT
	SET @ID_PIECES =(SELECT TOP(1) id FROM dbo.TB_PIECES  WHERE serialNumber = @Serial_Number )
	if (@ID_PIECES IS NOT NULL)
	BEGIN
		DECLARE @ID_RESULTS INT
		SET @ID_RESULTS = (SELECT TOP(1) id FROM dbo.TB_RESULTS WHERE TB_RESULTS.Fk_Pieces=@ID_PIECES AND TB_RESULTS.Fk_Results_Info =@Result_Id)
		if (@ID_RESULTS IS NULL)
		BEGIN
			INSERT dbo.TB_RESULTS
				(
					Fk_Pieces,
					Fk_Results_Info,
					value
				)
			VALUES
				(
					@ID_PIECES,
					@Result_Id,
					@Result_Value
				)
		END
		ELSE
		BEGIN
			UPDATE dbo.TB_RESULTS
			SET TB_RESULTS.value = @Result_Value 
			WHERE TB_RESULTS.id= @ID_RESULTS 
		END
	END

END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_Results_Values]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[stp_Set_Results_Values]
		@Serial_Number VARCHAR(100),
		@Job_Id BIGINT,
		@Results Result READONLY
AS
BEGIN
	DECLARE @ID_PIECES INT
	SET @ID_PIECES =(SELECT TOP(1) id FROM dbo.TB_PIECES  WHERE serialNumber = @Serial_Number )
if (@ID_PIECES IS NOT NULL)
	BEGIN
		DECLARE @ID_RESULTS INT
		DECLARE @Result_Id BIGINT
		DECLARE @Result_Value VARCHAR(200)

		declare cur CURSOR LOCAL for select Result_Id, Result_Value from @Results
		open cur
		fetch next from cur into @Result_Id, @Result_Value
		while @@FETCH_STATUS = 0 
		BEGIN

			SET @ID_RESULTS = (SELECT TOP(1) id FROM dbo.TB_RESULTS WHERE TB_RESULTS.Fk_Pieces=@ID_PIECES AND TB_RESULTS.Fk_Results_Info =@Result_Id)

			if (@ID_RESULTS IS NULL)
			BEGIN
				INSERT dbo.TB_RESULTS
					(
						Fk_Pieces,
						Fk_Results_Info,
						Fk_Jobs,
						value
					)
				VALUES
					(
						@ID_PIECES,
						@Result_Id,
						@Job_Id,
						@Result_Value
					)
			END
			ELSE
			BEGIN
				UPDATE dbo.TB_RESULTS
				SET TB_RESULTS.value = @Result_Value, TB_RESULTS.Fk_Jobs = @Job_Id
				WHERE TB_RESULTS.id= @ID_RESULTS 
			END

			fetch next from cur into @Result_Id, @Result_Value
		END
		close cur
		deallocate cur
	END

END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_Station_IsExcluded]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_Station_IsExcluded]
	-- Add the parameters for the stored procedure here
		@Station_Id BIGINT,
		@IsExcluded BIT
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)id FROM dbo.TB_STATIONS_CONFIG WHERE id = @Station_Id)

	IF(@ID IS NOT NULL)
		BEGIN
			SET @ID = (SELECT TOP(1)TB_STATIONS_STATUS.Fk_Stations FROM dbo.TB_STATIONS_STATUS WHERE Fk_Stations = @Station_Id)

			IF(@ID IS NULL)
				INSERT TB_STATIONS_STATUS
				(
					TB_STATIONS_STATUS.Fk_Stations,
					TB_STATIONS_STATUS.Excluded
				)
				VALUES
				(
					@Station_Id,
					@IsExcluded 
				)
			BEGIN
				UPDATE TB_STATIONS_STATUS
				SET TB_STATIONS_STATUS.Excluded = @IsExcluded
				WHERE Fk_Stations = @Station_Id
			END
		END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_StationDescription]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_StationDescription]
	-- Add the parameters for the stored procedure here
		@Station_Id BIGINT,
		@Description VARCHAR(50)

AS
BEGIN

	UPDATE TB_STATIONS_CONFIG SET description = @Description WHERE id = @Station_Id 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_User_Level]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_User_Level]
	-- Add the parameters for the stored procedure here
		@User_Name VARCHAR(50),
		@User_Level TINYINT
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)id FROM dbo.TB_USERS WHERE name  = @User_Name)
	IF(@ID IS NOT NULL)
	BEGIN
		UPDATE dbo.TB_USERS 
		SET TB_USERS.userLevel = @User_Level
		WHERE TB_USERS.id = @ID 			
	END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_User_Password]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_User_Password]
	-- Add the parameters for the stored procedure here
		@User_Name VARCHAR(50),
		@User_Password VARCHAR(50)
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)id FROM dbo.TB_USERS WHERE name  = @User_Name)
	IF(@ID IS NOT NULL)
	BEGIN
		UPDATE dbo.TB_USERS 
		SET TB_USERS.password = @User_Password
		WHERE TB_USERS.id = @ID 			
	END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Set_Vehicle_State]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Set_Vehicle_State]
	-- Add the parameters for the stored procedure here
		@Vehicle_Tag VARCHAR(50),
		@IsExcluded BIT,
		@NeedMaintenance BIT OUT,
		@Part_Number VARCHAR(100),
		@ForceGotoStationId BIGINT
AS
BEGIN
    DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)id FROM dbo.TB_VEHICLES_STATUS WHERE tag = @Vehicle_Tag)

	DECLARE @ID_PARTS AS BIGINT
	if (@Part_Number IS NULL OR @Part_Number = '')
		BEGIN
			SET @ID_PARTS =0
		END
	ELSE
		BEGIN
			SET @ID_PARTS =(SELECT TOP(1)Id_Parts FROM dbo.TB_PARTS WHERE PartNumber = @Part_Number)
		END

	IF(@ID IS NULL)
		BEGIN
			INSERT TB_VEHICLES_STATUS
			(
				TB_VEHICLES_STATUS.tag,
				TB_VEHICLES_STATUS.excluded,
				TB_VEHICLES_STATUS.NeedMaintenance,
				TB_VEHICLES_STATUS.Fk_Parts,
				TB_VEHICLES_STATUS.ForceGoToStationId 
			)
			VALUES
			(
				@Vehicle_Tag,
				@IsExcluded,
				@NeedMaintenance,
				@ID_PARTS,
				@ForceGotoStationId
			)
		END
	ELSE
		BEGIN
			UPDATE TB_VEHICLES_STATUS
			SET TB_VEHICLES_STATUS.excluded = @IsExcluded,
			    TB_VEHICLES_STATUS.NeedMaintenance = @NeedMaintenance,
			    TB_VEHICLES_STATUS.Fk_Parts = @ID_PARTS
			WHERE id = @ID
		END	 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_Unload_Piece]    Script Date: 11/30/2020 4:33:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_Unload_Piece]
	-- Add the parameters for the stored procedure here
		@Serial_Number VARCHAR(50)
AS
BEGIN
	
	DECLARE @ID AS BIGINT
	SET @ID =(SELECT TOP(1)id FROM dbo.TB_PIECES WHERE TB_PIECES.serialNumber = @Serial_Number)

	IF(@ID IS NOT NULL)
	BEGIN
		UPDATE dbo.TB_PIECES 
		SET TB_PIECES.vehicleTag = '',
			TB_PIECES.UnloadDate = GetDATE(),
			TB_PIECES.LastUpdate = GetDATE()
		WHERE TB_PIECES.id = @ID
	END
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[31] 4[31] 2[21] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TB_PARTS"
            Begin Extent = 
               Top = 6
               Left = 489
               Bottom = 119
               Right = 659
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TB_PARAMETERS_VALUES"
            Begin Extent = 
               Top = 6
               Left = 260
               Bottom = 136
               Right = 451
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TB_PARAMETERS_INFO"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 222
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1770
         Alias = 2595
         Table = 3525
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V000_Parameter_Values'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V000_Parameter_Values'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[52] 4[10] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TB_PIECES"
            Begin Extent = 
               Top = 33
               Left = 502
               Bottom = 163
               Right = 692
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TB_RESULTS"
            Begin Extent = 
               Top = 27
               Left = 255
               Bottom = 169
               Right = 445
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TB_RESULTS_INFO"
            Begin Extent = 
               Top = 28
               Left = 19
               Bottom = 196
               Right = 209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V001_Result_Values'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V001_Result_Values'
GO
USE [master]
GO
ALTER DATABASE [FM_35710] SET  READ_WRITE 
GO
